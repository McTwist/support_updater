@echo off
::#########
:: Blockland windows zip creator
::#########
:: This file is run to create a zip file in Windows
::
:: Requirements:
:: 7zip (Windows does not include any native form to create a zip file)
:: 7zip path need to be placed in Path environment variable

:: Set up some variables
set ZIP_NAME=Support_Updater.zip

:: Run the command
7z a -mx9 -tzip -r .\%ZIP_NAME% . -x!*.bat -x!*.*~ -x!.git -x!Support_Updater.zip